## Table of Contents

- [Introduction](#introduction)
- [Setup](#setup)
- [Features](#features)
- [Feedback](#feedback)
- [Contributors](#contributors)

## Introduction

This project is in support of research by Automation Guild task to understand the GitLab CI CD feature architecture and features

## Setup

In the GitLab Settings/CICD - expand the Varibles and add the following;
* AWS_ACCESS_KEY_ID = your_AWS_ACCESS_KEY_ID and ensure it is masked
* AWS_SECRET_ACCESS_KEY = your_AWS_SECRET_ACCESS_KEY and ensure it is masked

## Features

A few of the things you can do with GitPoint:

* Verify AWS connectivity via terraform
* Create via "terraform plan"
* Deploy new infrastructure via "terraform apply"
* Destroy infrastructure associcated with this environment via "terraform destory"

## Feedback

Feel free to send us feedback on [Confluence](https://escmconfluence.1dc.com/display/LKB/Fiserv+-+GitLab+research+for+Automation+Guild) or [file an issue](https://gitlab.com/mjlambert1222/prjxxx-simple-server/-/issues/new). Feature requests are always welcome. 

## Contributors

* Paul Orams paul.orams@fiserv.com
* Rakesh Menon
* Thomas Hickey thomas.hickey@fiserv.com
* Ivan Ivanov


## Build Process

https://escmconfluence.1dc.com/display/LKB/GitLab+CI+CD+pipeline+-+hands+on+exercise
